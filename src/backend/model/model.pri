HEADERS += \
    $$PWD/Filters.h \
    $$PWD/Meta.h \
    $$PWD/System.h \

SOURCES += \
    $$PWD/Filters.cpp \
    $$PWD/Meta.cpp \
    $$PWD/System.cpp \

include(gaming/gaming.pri)
include(settings/settings.pri)
