HEADERS += \
    $$PWD/Locale.h \
    $$PWD/LocaleList.h \
    $$PWD/Settings.h \
    $$PWD/Theme.h \
    $$PWD/ThemeList.h \

SOURCES += \
    $$PWD/Locale.cpp \
    $$PWD/LocaleList.cpp \
    $$PWD/Settings.cpp \
    $$PWD/Theme.cpp \
    $$PWD/ThemeList.cpp \
