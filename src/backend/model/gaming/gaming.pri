HEADERS += \
    $$PWD/Collection.h \
    $$PWD/CollectionList.h \
    $$PWD/Game.h \
    $$PWD/GameAssets.h \
    $$PWD/GameList.h \

SOURCES += \
    $$PWD/Collection.cpp \
    $$PWD/CollectionList.cpp \
    $$PWD/Game.cpp \
    $$PWD/GameAssets.cpp \
    $$PWD/GameList.cpp \
