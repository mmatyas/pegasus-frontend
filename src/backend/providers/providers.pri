HEADERS += \
    providers/Provider.h \
    providers/es2/Es2Metadata.h \
    providers/es2/Es2Provider.h \
    providers/es2/Es2Systems.h \
    providers/pegasus/PegasusCollections.h \
    providers/pegasus/PegasusCommon.h \
    providers/pegasus/PegasusMetadata.h \
    providers/pegasus/PegasusProvider.h \
    providers/steam/SteamGamelist.h \
    providers/steam/SteamMetadata.h \
    providers/steam/SteamProvider.h \

SOURCES += \
    providers/Provider.cpp \
    providers/es2/Es2Metadata.cpp \
    providers/es2/Es2Provider.cpp \
    providers/es2/Es2Systems.cpp \
    providers/pegasus/PegasusCollections.cpp \
    providers/pegasus/PegasusCommon.cpp \
    providers/pegasus/PegasusMetadata.cpp \
    providers/pegasus/PegasusProvider.cpp \
    providers/steam/SteamGamelist.cpp \
    providers/steam/SteamMetadata.cpp \
    providers/steam/SteamProvider.cpp \
