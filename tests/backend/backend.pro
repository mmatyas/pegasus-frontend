TEMPLATE = subdirs

SUBDIRS += \
    api \
    configfile \
    favorites \
    filters \
    model \
    providers \
    utils \
